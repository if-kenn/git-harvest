'use strict';
/* jshint browser: true */

/**
 * Sets and injects frame.html into the DOM.
 *
 * @return {[type]} [description]
 */
function injectHarvestFrame() {
  chrome.storage.sync.get(['hosts'], function(items) {
    var url = window.location.href,
        urlRegex, matches, gitPath, issue, name, title, search, wrapper, frameURL, iframe;

    if (items && typeof items.hosts !== 'undefined') {
      // Parse the path and gather parameters to be passed to Harvest
      urlRegex = new RegExp('^(' + items.hosts.join('|').replace(/\./g, '\\.') + ')((?:\/[a-zA-Z0-9\\._-]+){2,})\/(issues|merge_requests)\/(\\d+)', 'i');
      matches = url.match(urlRegex);

      // Make sure the host is approved and it is on issues
      if (matches) {
        gitPath = matches[2];
        issue = matches[4];

        // logic for getting github info
        if (window.location.host != 'github.com') {
          search = document.getElementById('search_project_id');
          name = search.dataset.name;
          title = document.getElementsByTagName('h2')[1].innerHTML;
          wrapper = document.getElementsByClassName('content')[0];
        } else {
          title = document.getElementsByClassName('js-issue-title')[0].innerHTML.replace(/^\s+(.*)?\s+$/g, '$1');
          name = document.getElementsByClassName('gh-header-number')[0].innerHTML;
          wrapper = document.getElementById('discussion_bucket');
        }

        // if we have something to append to
        if (wrapper) {
          frameURL = 'frame.html?';
          frameURL += 'issue=' + issue;
          frameURL += '&url=' + encodeURIComponent(url);
          frameURL += '&gitPath=' + encodeURIComponent(gitPath);
          frameURL += '&title=' + encodeURIComponent(title);
          frameURL += '&name=' + encodeURIComponent(name);

          // Embed the harvest iframe at the bottom of the .content div.
          iframe = document.createElement('iframe');
          iframe.src = chrome.runtime.getURL(frameURL);
          iframe.style.cssText = 'width:100%;height:450px;border:0';

          wrapper.appendChild(iframe);
        }
      }
    }
  });
}

// Do it!
injectHarvestFrame();
