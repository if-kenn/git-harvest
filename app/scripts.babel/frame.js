'use strict';
/* jshint browser: true */

chrome.storage.sync.get(['projects', 'show_widget'], function(conf) {
  var toggle = document.getElementById('gitlab-harvest-toggle');

  toggle.addEventListener('click', function () {
    harvestState(conf, toggle);
  });

  // get state from options if available
  if (typeof conf !== 'undefined' && typeof conf.show_widget !== 'undefined' && conf.show_widget == 'yes') {
    harvestState(conf, toggle, 'on');
  }
});

function harvestState(conf, toggle, toggleState) {
  var iframe = document.getElementById('gitlab-harvest-wrapper');

  if (typeof toggleState === 'undefined') {
    toggleState = iframe.innerHTML.length > 0 ? 'off' : 'on';
  }

  toggle.innerHTML = '<img src="images/harvest-icon-' + toggleState + '.png" width="32" height=" 32" alt="Turn Harvest Time Tracking ' + toggleState + '" />';

  if (toggleState == 'on') {
    iframe.innerHTML = '<iframe width="100%" height="400" frameborder="0" src="' + buildHarvestURL(conf) + '"  ></iframe>';
  } else {
    iframe.innerHTML = '';
  }
}

function buildHarvestURL(conf) {
  // Gather variables passed to this page via query string.
  var gitPath = decodeURIComponent(getParameterByName('gitPath')),
      issue = '#' + getParameterByName('issue'),
      name = getParameterByName('name'),
      code = '',
      harvestFrameURL = 'https://platform.harvestapp.com/platform/timer?',
      params = {
        app_name:           'Gitlab',
        external_item_id:   gitPath + issue,
        external_item_name: issue + ': ' + getParameterByName('title'),
        permalink:          getParameterByName('url'),
        closable:          'false'
      },
      queryArr = [];

  // default_project_code
  if (typeof conf !== 'undefined' && typeof conf.projects !== 'undefined') {
    for (var curr_group_proj in conf.projects) {
      if (
        conf.projects.hasOwnProperty(curr_group_proj) &&
        gitPath == '/' + curr_group_proj
      ) {
        code = conf.projects[curr_group_proj];
        params.default_project_code = conf.projects[curr_group_proj];
      }
    }
  }

  // Build the Harvest timer widget iframe query params
  for (var key in params) {
    if (params.hasOwnProperty(key)) {
      queryArr.push(encodeURIComponent(key) + '=' + encodeURIComponent(params[key]));
    }
  }

  // favor code over name
  if (code != '') {
    queryArr.push('default_project_code=' + encodeURIComponent(code));
  } else {
    queryArr.push('default_project_name=' + encodeURIComponent(getParameterByName('name')));
  }

  return harvestFrameURL + queryArr.join('&');
}

/**
 * Fetches URL paramaters from the query string.
 * @param  {string} name The query string variable key.
 * @return {string}      The query parameter value.
 */
function getParameterByName(name) {
  var url = window.location.href,
      regex = new RegExp('[?&]' + name.replace(/[\[\]]/g, '\\$&') + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);

  if (!results) return null;
  if (!results[2]) return '';

  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
