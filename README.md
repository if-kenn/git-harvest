# Google Chrome Extension for Harvest time tracking widget for GitLab

A Google Chrome extension that provides [Harvest](https://www.getharvest.com/) time tracking via the [Harvest Time Tracking Widget](https://github.com/harvesthq/platform/blob/master/widget.md) by embedding it on [GitLab](https://gitlab.com) issue pages.

This is available as an installable extension in the [Google Chrome Web Store](https://chrome.google.com/webstore/detail/gitlab-harvest/ofbfhaiknhlanbliolbkiidknakaldmo)

![Gitlab Harvest Time Tracking for Google Chrome Screenshot](https://gitlab.com/blueoakinteractive/gitlab-harvest/raw/master/app/images/gitlab-harvest.png "Gitlab Harvest Time Tracking for Google Chrome Screenshot")

This extension was built with the help of the [Yeoman Crome Extension Generator](https://github.com/yeoman/generator-chrome-extension)

## Disclaimer

There are no guarantees or warranties with this app.

This app is built by (Blue Oak Interactive)[https://www.blueoakinteractive.com] which is not affiliated with GitLab or Harvest.

## Ideas?

Have ideas on how to make this better? Issues and merge requests are welcome and encouraged!
